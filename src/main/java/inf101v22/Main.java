package inf101v22;

import java.util.Arrays;

public class Main {
	
	public static void main(String[] args) {
		System.out.println("Hello World");

		Integer a = 2;
		Integer b = 4;
		Integer c = 3;

		Integer[] myNums = { a, b, c };
		Integer[] yourNums = { a, b, c };
		Integer[] theirNums = yourNums;

		System.out.println(a.equals(myNums[0]));
		System.out.println(arraysAreEquals(myNums, yourNums));
		System.out.println(arraysAreEquals(yourNums, theirNums));



		Boolean[][] myTetrisPiece = new Boolean[][] {
			{ true, true, true, },
			{ false, true, false, },
		};
		Boolean[][] yourTetrisPiece = new Boolean[][] {
			{ true, true, true, },
			{ false, true, false, },
		};

		System.out.println(arraysAreEquals(myTetrisPiece, yourTetrisPiece));
		System.out.println(Arrays.deepEquals(myTetrisPiece, yourTetrisPiece));

	}

	static Boolean[][] copy(Boolean[][] source) {
		int length = source.length;
		Boolean[][] target = new Boolean[length][];

		for (int i = 0; i < target.length; i++) {
			target[i] = new Boolean[source[i].length];
			for (int j = 0; j < target.length; j++) {
				target[i][j] = source[i][j];
			}
		}

		return target;
	}

	static boolean arraysAreEquals(Object[] a, Object[] b) {
		if (a == b) {
			return true;
		}
		if (a == null || b == null) {
			return false;
		}
		if (a.length != b.length) {
			return false;
		}

		for (int i = 0; i < b.length; i++) {
			Object ai = a[i];
			Object bi = b[i];

			if (ai instanceof Object[] && bi instanceof Object[]) {
				Object[] aCasted = (Object[]) ai;
				Object[] bCasted = (Object[]) bi;
				return arraysAreEquals(aCasted, bCasted);
			}

			if (!ai.equals(bi)) {
				return false;
			}
		}

		return true;
	}

}